from pyraytracer.frame import Frame
from pyraytracer.spectrum import Spectrum


def render():
    frame = Frame(width=5, height=5)
    frame.contribute_at(Spectrum.black(), 0, 0)
    frame.contribute_at(Spectrum.red(), 0, 1)
    frame.contribute_at(Spectrum.green(), 0, 2)
    frame.contribute_at(Spectrum.blue(), 0, 2)
    frame.contribute_at(Spectrum.blue(), 0, 3)
    frame.contribute_at(Spectrum.black(), 0, 4)
    frame.render()


if __name__ == '__main__':
    render()
