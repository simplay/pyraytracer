class Spectrum:
    def __init__(self, red: int, green: int, blue: int, alpha: int = 255):
        """
        :param red:
        :param green:
        :param blue:
        :param alpha:
        """

        self.r = red
        self.g = green
        self.b = blue
        self.a = alpha

    @classmethod
    def black(cls):
        return Spectrum(0, 0, 0)

    @classmethod
    def red(cls):
        return Spectrum(255, 0, 0)

    @classmethod
    def green(cls):
        return Spectrum(0, 255, 0)

    @classmethod
    def blue(cls):
        return Spectrum(0, 0, 255)

    def data(self) -> list:
        return [self.r, self.g, self.b, self.a]
