import numpy as np
from PIL import Image

from pyraytracer.spectrum import Spectrum


class Frame:
    def __init__(self, width: int, height: int):
        """
        :param width: number of width pixels
        :param height: number if height pixels
        """

        # dimensions: width x height x rgba-channels
        self.buffer = np.zeros(shape=(height, width, 4), dtype=np.float64)
        self.hit_counter = np.zeros(shape=(height, width), dtype=np.float64)

    def contribute_at(self, spectrum: Spectrum, row_idx: int, col_idx: int) -> None:
        self.buffer[row_idx][col_idx] += spectrum.data()
        self.hit_counter[row_idx][col_idx] += 1

    def render(self, show: bool = False) -> None:
        buffer_copy = self.buffer.copy()
        hit_record_copy = self.hit_counter.copy()
        hit_record_copy[hit_record_copy == 0] = 1

        buffer_copy[:, :, 0] /= hit_record_copy
        buffer_copy[:, :, 1] /= hit_record_copy
        buffer_copy[:, :, 2] /= hit_record_copy
        buffer_copy[:, :, 3] /= hit_record_copy

        buffer_copy = np.array(buffer_copy, dtype=np.uint8)
        image = Image.fromarray(buffer_copy, mode="RGBA")

        image.save("frame.png")

        if show:
            image.show()
